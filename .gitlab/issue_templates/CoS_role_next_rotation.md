# Work with People Group to promote and onboard next Acting Chief of Staff to the CTO

As the current Acting Chief of Staff to the CTO, you should help promote the role to help find the next team member for the rotation.

You may not be involved in the actual selection of the next Acting Chief of Staff to the CTO, as that would be up to the People Group and the CTO.
However, during the selection process, you should be available to answer questions.

### Promotion in Month 1

During the first month of your rotation, promote the role. This may include any number of things, but at minimum:

- [ ] Post in Slack #chief-of-staff-to-cto-alumni and #cto
- [ ] Post in Engineering Week in Review

Offer to answer any questions and do coffee chats.

Check with People Business Partner (and CTO). If there are very few candidates, you may want to consider other ways of promoting the role, such as organizing an AMA.

### Check-in in Month 2

In the second month, check in with the People Business Partner (and CTO) on timeline.

The goal is to follow the [handbook timeline](https://about.gitlab.com/handbook/engineering/cto-staff/#how-the-rotation-works):

1. Coffee chats with CTO and VPs in Month 2.
1. Announce the successful candidate for the following quarter's rotation by the end of Month 2 or beginning of Month 3.

### Onboard in Month 3

Once announced:

- [ ] Open onboarding issue for incoming Acting CoS

/label ~"repeat task" ~cto-cos-meta ~priority::3
/milestone %"FY24-Q1"
/due Thu Fri Mar 03 2023
/assign me
