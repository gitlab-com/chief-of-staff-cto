# FY24-Q1 Onboarding: [Member Name]

With the exception of scheduling some meetings, and some of the learning material, all other tasks should be completed by the end of Day 1.

## Meetings to create

* [ ] `Current Chief of Staff` task: Schedule handoff meeting with incoming Chief of Staff and CTO
* [ ] Introductory Coffee Chats with CTO. Ask `@Marisa Carlson` (EBA to CTO) to set up
* [ ] Weekly 1:1 with CTO. Work with `@Marisa Carlson` (EBA to CTO) to set up
* [ ] Introductory Coffee Chats with CTO's direct reports (check Workday org chart if unsure who they are)
* [ ] Regular 1:1 with CTO's direct reports (typically biweekly, but ask each person for frequency preference)
* [ ] Introductory Coffee Chat with EBA to CTO.
* [ ] Regular 1:1 with EBA to CTO. Suggest biweekly until mid-rotation, then weekly for offsite planning.

## Meetings to attend

Based on the discussions and access to the docs in these meetings, you will be added to the [Designated Insiders](https://about.gitlab.com/handbook/legal/publiccompanyresources/#designated-insiders) list.
If this is a concern, please speak to the CTO and the Legal team prior getting access.

* [ ] `Current Chief of Staff` task (EBA can also do this): Add you as an Owner to the Google Group for `office-of-the-cto@gitlab.com`

The Google Group for `office-of-the-cto@gitlab.com` is invited to all relevant meetings.
Once added, you should be invited to all the relevant meetings and have access to the agendas.

You will be invited to some Key Reviews that are not part of Engineering, but it's often good to attend
or at least review the agenda and slide deck to have a better idea of the company context.

<details>
<summary>See table of meetings (may be outdated, this is not the SSOT)</summary>

| Title                              | Day       | Time (UTC) | Frequency               | Agenda                                                                                         |
|------------------------------------|-----------|------------|-------------------------|------------------------------------------------------------------------------------------------|
| Engineering Allocation             | Tuesday   | 15:00      | Weekly                  | [Link](https://docs.google.com/document/d/164hNObllaLWosG110-A0UouYlcaqOxbPpHATFD38_Gw/edit)   |
| Engineering Leadership             | Wednesday | 22:30      | Weekly                  | [Link](https://docs.google.com/document/d/13kx3Yf9yGgS5bvV-vP3YTOh7xLT5vI0gG_ev3NjVnaM/edit)   |
| Engineering, Recruiting, FP&A Sync | Thursday  | 17:30      | Weekly                  | [Link](https://docs.google.com/document/d/19Bhl2AskuXJ0ufPZloNC_NTf5ZFadRq2SZKS4PjW77s/edit)   |
| Engineering Analytics              | Thursday  | 18:30      | Weekly                  | [Link](https://docs.google.com/document/d/1daqEon2jVKzd-dagxXEb1epNxn2v3soCx7n7s0tgfGk/edit)   |
| CTO Office Hours (APAC/AMER)       | Wednesday | 22:30      | Every 2 weeks           | [Link](https://docs.google.com/document/d/1UKAK51eyy7dOA9pRWz_VDEVhO6c2VBHoQ6MUB0RdvM8/edit)   |
| CTO Office Hours (EMEA/AMER)       | Thursday  | 16:00      | Every 2 weeks           | Same as above                                                                                  |
| CEO & Incubation Engineering       | Thursday  | 23:30      | Every 2 weeks           | [Link](https://docs.google.com/document/d/1Gihqwu4ceiwPBkZrRWzw586hfSP_LdbdV_4SsePDmHI/edit)   |
| R&D Variance                       | Wednesday | 16:30      | Monthly - 3rd Wednesday | [Link](https://docs.google.com/document/d/1kqeBXVqnTes2GYxVm5LwjqLGkAGSDuEgZ35gcrghXFA/edit)   |
| Support Key Review                 | Wednesday | 7:30       | Every 6 weeks           |                                                                                                |
| Product Key Review                 | Thursday  | 16:30      | Every 6 weeks           |                                                                                                |
| Infrastructure Key Review          | Thursday  | 6:00       | Every 6 weeks           |                                                                                                |
| Development Key Review             | Wednesday | 16:00      | Every 2 months          |                                                                                                |
| Quality Key Review                 | Thursday  | 17:30      | Every 2 months          |                                                                                                |
| Engineering All-Hands (APAC/AMER)  | Wednesday | 22:30      | Every 2 months          | [Link](https://docs.google.com/document/d/1rxU8btp15LyI4BYcW2SzjorJVxcWmK2yPYgxDcHhq38/edit) |
| Engineering All-Hands (EMEA/AMER)  | Thursday  | 16:00      | Every 2 months          | Same as above |
| OKR Retrospective                  | Tuesday   | 20:30      | Quarterly - 2nd Tuesday | [Link](https://docs.google.com/document/d/1OYoxv_5iD-\_kHujiq_6Zh9GAY8WX2FUJ1zqWzznH8RI/edit#) |

</details>

## Reminders to add

* [ ] Weekly: check <https://drive.google.com/drive/search?q=followup:actionitems> to ensure those action items aren't dropped
* [ ] Weekly: check [CTO Should Know Weekly doc](https://docs.google.com/document/d/1Elx_A4sZHpRSJ9zjVi4z2BE7JLgfQcfsLaCLyi_GG5I/edit) for updates
* [ ] Engineering Week-In-Review (EWIR)
  * [ ] Weekly (end of Thursday EMEA/AMER / start of Friday APAC): message to channels reminding people to add items to EWIR: #engineering-fyi ; All department channels have weekly Slack reminders: #development, #infrastructure-lounge, #quality, #support-team-chat, and #incubation-eng
  * [ ] Weekly: Check meeting agendas for `FOWIR` tagged items, especially Eng Leadership and add them to the EWIR.
  * [ ] Weekly (Friday/Monday): Organize the week in review including removing empty sections, adding prefix tags/bolding key phrases.
  * [ ] Yearly: At the start of a new calendar year, create a doc in the [Engineering Week-in-Review shared drive folder](https://drive.google.com/drive/folders/1fR98A8Zfk0eP7BqOp8CWn91aiN_zx9uy) to archive the previous year. See older docs for examples.
* [ ] Periodically: Update/create the issue templates with any changes to the role.

## Issues to create

* [ ] Create an issue corresponding to [any with the label ~"repeat task"](https://gitlab.com/gitlab-com/chief-of-staff-cto/-/issues?sort=updated_desc&state=all&label_name%5B%5D=repeat+task) from the previous quarter.
  * Note: Each of these issues should have a corresponding issue template in this repo. If the dates are not for your quarter, please run a [manual CI job](https://gitlab.com/gitlab-com/chief-of-staff-cto/-/pipelines/new).

## Access Requests

Tasks for `Current Chief of Staff`:

* [ ] Give access to `Admin` role in Ally.io
  * This may change in the future with https://gitlab.com/groups/gitlab-org/-/epics/8990
* [ ] Give Maintainer access to the `gitlab-com/chief-of-staff-cto/` project

## Learning

* [ ] Review the [CTO Staff page](https://about.gitlab.com/handbook/engineering/cto-staff/) if you haven't already.
* [ ] Review [the list of Engineering Departments](https://about.gitlab.com/handbook/engineering/#engineering-departments) to get a high-level understanding of who they are and what they do.
* [ ] Read about how [Engineering OKRs](https://about.gitlab.com/handbook/engineering/okrs/) work and the linked Company OKR page.
* [ ] Read agenda and/or watch recordings from the last engineering offsite. Links in [the agenda](https://docs.google.com/document/d/1JMtbSvXwDiVm4XrCcr6f0479RYBZISlmfBBo3aKCWck/edit).
* [ ] Read the agenda format which is used in some meetings: <https://about.gitlab.com/handbook/leadership/1-1/suggested-agenda-format/#hallway>

## Administrivia

* [ ] Update job title to add Acting CoS role
  * [ ] GitLab.com
  * [ ] Slack
  * [ ] Zoom
  * [ ] Team page
* [ ] Remove from any on-call rotations for the quarter
* [ ] Remove from any other regular tasks that take significant time
* [ ] Comment on [the latest company newsletter content issue](https://gitlab.com/gitlab-com/internal-communications/newsletter/-/issues) to remove the previous Acting Chief of Staff and list you instead. For example: <https://gitlab.com/gitlab-com/internal-communications/newsletter/-/issues/25#note_1157810428>
* [ ] Add self to list of previous Chiefs of Staff: <https://about.gitlab.com/handbook/engineering/cto-staff/#rotation-history>
* [ ] When listing backup for OOO, consider no backup role, or multiple backup roles depending on the task.

## Slack channels

(Required) Please make sure to join these Slack channels (if you're not already there):

- [ ] `Current Chief of Staff` task (EBA can also do this): #cto-directs
- [ ] #cto
- [ ] #chief-of-staff-to-cto-and-alumni
- [ ] #okr_champions
- [ ] #engineering-fyi
- [ ] #eba-team [to (re)schedule meetings with e-group/direct reports](https://about.gitlab.com/handbook/eba/)

(Highly recommended) These channels should be used to communicate with individual VPs (when DMs are not required):
- [ ] #vp-development
- [ ] #incubation-eng
- [ ] #vp-infrastructure
- [ ] #quality-managers
- [ ] #spt-managers (Support)

(Optional) These channels are useful for following what's happening in Engineering or GitLab:
- [ ] #development
- [ ] #infrastructure-lounge
- [ ] #quality
- [ ] #support-team-chat
- [ ] #is-this-known
- [ ] #okrs
- [ ] #ceo
- [ ] #e-group

/label ~"repeat task" ~cto-cos-meta ~priority::3
/milestone %"FY24-Q1"
/due Mon Tue Feb 14 2023
