# Nudge people on OKRs without recent updates

We'd like people to be checking in on their KRs regularly so the status is always up to date, not just at the end of the quarter.

You can use sort the [Engineering OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=created_date&state=opened&label_name%5B%5D=division%3A%3AEngineering) by last updated date and lave a comment for the DRI to update the OKR.

/label ~"repeat task" ~OKR ~priority::3
/milestone %"FY24-Q1"
/due Fri Sat Mar 04 2023
/assign me
