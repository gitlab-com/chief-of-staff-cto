# Review OKRs for wording and alignment

We want to review the OKRs with a bit of distance.
The CoS to the CTO is a good candidate as they won't be able to fill in any missing departmental context.
We want to know:

1. Are the OKRs worded in such a way that they will be understandable at the end of the quarter?
    - [Handbook section on OKR criteria and wording formula](https://about.gitlab.com/company/okrs/#fundamentals-of-impactful-okrs)
1. Are the alignments set up correctly so that the right things are contributing to the scoring? If any of the VP (O)KRs don't align to an Engineering (CTO) (O)KR, should they?
1. Does the scoring match what the sub-OKRs say in their titles? Do those sound like they're related to the same thing?
1. Are any of the Engineering (CTO) OKRs missing sub-OKRs, do they need some?
1. Are the Engineering OKRs aligned with CEO OKRs? If unsure, check with the Chief of Staff to CEO team in the #okr_champions Slack channel.

This should start from the department KRs and work down, then ensure that everything is correctly aligned to Engineering KRs.

You can find the all the Engineering OKRs in this view in GitLab: https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=created_date&state=opened&label_name%5B%5D=division%3A%3AEngineering

* [ ] (Customer) Support
* [ ] Development
* [ ] Incubation
* [ ] Infrastructure
* [ ] Quality
* [ ] Engineering

Note: Consider including the name of the DRI in comments, and a list of mentioned DRIs for departments with more OKRs, especially Development.

For an example, see #54+

Once done:

1. [ ] On each OKR, mention the DRI with your comment(s).
1. [ ] Bring up any comments that require VP attention at the next 1:1 with VPs.
1. [ ] Update the 1:1 docs OKR tables.

/label ~"repeat task" ~OKR ~priority::2
/milestone %"FY24-Q1"
/due Mon Tue Feb 14 2023
/assign me
