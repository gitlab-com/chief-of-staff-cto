# Remind Engineering departments to share any upcoming OKRs with Product that need scheduling support

From the [handbook](https://about.gitlab.com/handbook/engineering/okrs)

> As a result, Engineering will communicate with Product **6 weeks before the start of the quarter** for any upcoming OKRs that need scheduling assistance from PMs. This is earlier than the typical company timeline for OKRs, but should not be a large proportion of Engineering OKRs in any quarter anyway.

We need to remind the relevant departments (mostly Development) as this time approached.

/label ~"repeat task" ~OKR ~priority::2
/milestone %"FY24-Q1"
/due Thu Fri Mar 17 2023
/assign me
