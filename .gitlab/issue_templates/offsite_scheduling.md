# Offsite Scheduling

## Details

<!-- -->

## Tasks

- [ ] In-person or **virtual**?
- [ ] Schedule a time that works for most people

/label ~"repeat task" ~offsite ~priority::1
/milestone %"FY24-Q1"
/due Fri Sat Mar 04 2023
/assign me
