# FY24-Q1 OKR Kickoff

Start the OKR process for FY24-Q1.

- [ ] Handle shared OKRs with Product: <https://about.gitlab.com/handbook/engineering/okrs/#okrs-that-require-product-to-schedule-work>
- [ ] Ask people to pass this to their teams to start working bottom-up
- [ ] Check with Product about not assigning joint Engineering OKRs to PMs, like SUS impact OKR
- [ ] Once finalized, add top-level Engineering OKRs to GitLab
- [ ] Let Direct Reports know in #cto or via Eng weekly meeting once added so they can add their own

/label ~priority::2 ~"repeat task" ~OKR
/milestone %"FY24-Q1"
/due Sat Sun Apr 16 2023
/assign me
