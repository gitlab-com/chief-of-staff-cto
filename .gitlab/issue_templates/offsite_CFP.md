# Offsite Call For Proposals

For the offsite (<https://about.gitlab.com/handbook/engineering/cto-staff/#engineering-offsite>), we say:

> In general, we prefer to have more items than we can fit into the agenda, and then push some items out, rather than have too little.

So we should canvas the different Engineering departments (plus recruiting and people) for topics for this offsite.

/label ~"repeat task" ~offsite ~priority::1
/milestone %"FY24-Q1"
/due Thu Fri Mar 17 2023
/assign me
