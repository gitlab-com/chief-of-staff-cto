# FY24-Q1 Offboarding: [Member Name]

- [ ] Remove meetings from calendars
  - [ ] CTO 1:1
  - [ ] 1:1s with CTO's Directs
- [ ] Remove yourself from the `office-of-the-cto@gitlab.com` Google group
- [ ] Request [removal from Designated Insider list](https://drive.google.com/drive/search?q=Designated%20Insider%20Pre-Clearance%20Process%20%26%20FAQ) ("How can I be removed from the Designated Insider list?"), if applicable
- [ ] Update job title back to your regular position
  - [ ] GitLab.com
  - [ ] Slack
  - [ ] Zoom
  - [ ] Team page (remove addition)
- [ ] Unshare PTO Roots with CTO (if applicable)
- [ ] Close any remaining issues in this project
- [ ] 

/label ~"repeat task" ~cto-cos-meta ~priority::3
/milestone %"FY24-Q1"
/due Tue Wed May 03 2023
/assign me
