# Offsite - Organize Agenda

Agenda document: (<https://docs.google.com/document/d/1dNiT_wyFIL5Jp8fRz_PwJAQdWcoabQPKhvp_7-f6VZ0/edit>)

/label ~"repeat task" ~offsite ~priority::1
/milestone %"FY24-Q1"
/due Sun Mon Apr 03 2023
/assign me
