# FY24-Q1 Retrospective

Retrospective for the Acting Chief of Staff

Please look at back at your experiences and ask yourself:

- **:star2: What praise do you have for others?**
- **:thumbsup: What went well?**
- **:thumbsdown: What didn’t go well?**
- **:chart_with_upwards_trend: What can we improve going forward?**

Also ask the CTO and CTO directs for their thoughts:

- **:star: Could you highlight 1-2 things that the Acting Chief of Staff to CTO did?** The highlight(s) could be anything, such as something you appreciated, or found impactful.
- **:thought_balloon: Anything that we can improve in the role going forward?** As this issue is public, following [our values](https://about.gitlab.com/handbook/values/#negative-feedback-is-1-1), if you have personal feedback that may be viewed as negative, please reach out directly to the Acting Chief of Staff.

/label ~"repeat task" ~cto-cos-meta ~priority::3 ~retrospective
/milestone %"FY24-Q1"
/due Tue Wed May 03 2023
/assign me
