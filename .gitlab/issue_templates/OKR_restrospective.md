# Previous quarter OKR retrospective

From the [handbook](https://about.gitlab.com/handbook/engineering/okrs/#okr-retrospection)

> The Chief Technology Officer and the leaders of each department meet synchonously on the second Tuesday in the month after each quarter ends to discuss the OKRs from the previous quarter.

The retrospective should already be scheduled, and your role is to help with prep and "running" it.

1. [ ] **At least 1 week prior** [Create the outline from the template](https://docs.google.com/document/d/1OYoxv_5iD-_kHujiq_6Zh9GAY8WX2FUJ1zqWzznH8RI/edit#) if not already done. Copy/paste the larger part as many times as needed for the number of departments, and add the "Overall" at the end.
1. [ ] Get people to populate the meeting doc. Ping the direct reports in the #cto channel.
1. [ ] Send a reminder 1-2 days before to those that haven't filled out the doc.
1. [ ] Organize the agenda as needed prior to the meeting. This can include rearranging the order of departments as needed (for instance, if someone can only join late, their department shouldn't go first).
1. [ ] If present during the call, help keep people on time (check top of the doc for guidance). If you cannot be present, ask someone else to help keep time.


/label ~"repeat task" ~OKR ~priority::1
/milestone %"FY24-Q1"
/due Tue Wed Feb 08 2023
/assign me
