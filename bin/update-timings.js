const fs = require("fs");
const path = require("path");

const MATCHES = {
  QUARTER: /FY\d\d\s?-?Q\d/gm,
  DATE: /[a-z]{3}\s\d{2}\s\d{4}/gim,
  DAYS: /days?/i,
  WEEKS: /weeks?/i,
  MONTHS: /months?/i,
};

function getCurrentQuarterString() {
  const startOfQuarter = getStartOfQuarter();
  const fiscalYear = startOfQuarter.getFullYear() - 1999;
  const quarter = (startOfQuarter.getMonth() + 2) / 3;

  return `FY${fiscalYear}-Q${quarter}`;
}

function getStartOfQuarter(date = new Date()) {
  let year = date.getFullYear();
  let month = date.getMonth();

  if (month < 1) {
    year--;
    month = 10;
  } else if (month < 4) {
    month = 1;
  } else if (month < 7) {
    month = 4;
  } else if (month < 10) {
    month = 7;
  } else if (month < 12) {
    month = 10;
  }

  return new Date(year, month, 1);
}

function getDistanceInDays(date) {
  const startOfQuarter = getStartOfQuarter(date);
  console.log(startOfQuarter);
  return Math.round(
    (date.getTime() - startOfQuarter.getTime()) / 1000 / 60 / 60 / 24
  );
}

function parseDate(dateStr) {
  const startOfCurrentQuarter = getStartOfQuarter();
  const date = new Date(dateStr);
  const distance = getDistanceInDays(date);
  const newDate = new Date(
    startOfCurrentQuarter.setDate(startOfCurrentQuarter.getDate() + distance)
  );
  return newDate.toDateString();
}

async function getFileContents(path) {
  return new Promise((res, rej) => {
    fs.readFile(path, "utf8", (err, data) => {
      if (err) {
        rej(err);
      }
      res(data);
    });
  });
}

async function parseFile(file) {
  const fileContents = await getFileContents(file);
  const currentQuarter = getCurrentQuarterString();

  const newString = fileContents
    .replace(MATCHES.QUARTER, currentQuarter)
    .replace(MATCHES.DATE, parseDate);

  fs.writeFile(file, newString, function (err) {
    if (err) {
      return console.error(err);
    }
    console.log("file updated", file);
  });
}

function updateIssueTemplates() {
  const dir = path.join(__dirname, "../.gitlab/issue_templates/");
  fs.readdir(dir, (err, filenames) => {
    if (err) {
      console.error(err);
      return;
    }
    filenames.forEach((filename) => {
      parseFile(dir + filename);
    });
  });
}

updateIssueTemplates();
