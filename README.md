# Chief of Staff to the CTO

This project will house all the issues that the Chief of Staff to the CTO is currently working on.
There are issue templates in the repo to help with setting these issues up.
If you need to be added to the project, please reach out to a current or past Acting Chief of Staff to the CTO, or the CTO directly.
